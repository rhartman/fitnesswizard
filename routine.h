#include "stdafx.h"
#include <vector>
#include <string>

using namespace std;

struct exercise{
	exercise()
	{
		name = "";
		currentSet = 0;
		/*results1.push_back(0);
		results2.push_back(0);*/
		//results1Goals = 0;
		//results2Goals = 0;
		description = "";
	}
	string name;
	bool cardio; // if not cardio then weight lifting
	int currentSet; // keep track off which set they're on
	// Results of the current exercise
	vector<int> results1; // if cardio then time if weight then # of reps
	vector<int> results2; // if cardio then distance if weight then amount of weight
	// Goals calculated on the user's previous routine results
	//int results1Goals; // if cardio then time if weight then # of reps ***Not Used
	//int results2Goals; // if cardio then distance if weight then amount of weight ***Not Used
	string description; // brief instructions
};

struct workout{
	workout()
	{
		currExercise = 0;
		date = "00/00/00";
	}
	vector<exercise> w;
	int currExercise;
	string date;
};

class routine
{
public:
	//char date[9];
	vector<workout> r;
	int currWorkout;
};