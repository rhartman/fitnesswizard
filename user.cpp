#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

//Global variables need to point somewhere in order to be used, NULL is used to make GetInstance call the constructor on the first call.
user* user::singleton = NULL;

//Declare come functions
void ParseRoutine(ifstream &file, routine &tempRoutine, string &line, string &key, string &value);
void ParseWorkout(ifstream &file, workout &tempWorkout, string &line, string &key, string &value);
void ParseExercise(ifstream &file, exercise &tempExercise, string &line, string &key, string &value);

//using namespace std;
//Default Constructor
user::user()
{
	age = 0;
	weightz.push_back(0);
	goals = 0; 
	fitLevel = 0;
	height.ft = 0;
	height.in = 0;
	userName = "";
	password = "";
	bmi = 0;
	intensity = 0;
}

user* user::GetInstance()
{
	if(user::singleton == NULL)
	{
		user::singleton = new user;
	}
	return singleton;
}

void user::save()
{
	GetInstance()->CalcBMI();
	ofstream of;
	of.open(GetInstance()->GetUsername());
	of << "username=" << GetInstance()->GetUsername() << endl;
	of << "password=" << GetInstance()->GetPassword() << endl;
	of << "age=" << GetInstance()->GetAge() << endl;
	of << "initWeight=" << GetInstance()->GetInitWeight() << endl;
	of << "height_ft=" << GetInstance()->GetHeightFt() << endl;
	of << "height_in=" << GetInstance()->GetHeightIn() << endl;
	of << "fitLevel=" << GetInstance()->GetFitLevel() << endl;
	of << "goals=" << GetInstance()->GetGoals() << endl;
	of << "bmi=" << setiosflags(ios::fixed) << setprecision(2) << GetInstance()->bmi << endl;
	of << "intensity=" << GetInstance()->CalcIntensity() << endl << endl;
	
	if(currRoutine.currWorkout>=0)
	{
		of << "CURRROUTINE" << endl;
		of << "\tcurrWorkout=" << currRoutine.currWorkout << endl;
		for(std::vector<workout>::iterator workout = currRoutine.r.begin(); workout != currRoutine.r.end(); ++workout) {
			of << "\tWORKOUT" << endl;
			of << "\t\tcurrExercise=" << workout->currExercise << endl;
			of << "\t\tdate=" << workout->date << endl;
			for(std::vector<exercise>::iterator ex = workout->w.begin(); ex != workout->w.end(); ++ex)
			{
				of << "\t\tEXERCISE" << endl;
				of << noskipws << "\t\t\tname=" << ex->name << endl;
				of << "\t\t\tcardio=" << ex->cardio << endl;
				of << "\t\t\tcurrentSet=" << ex->currentSet << endl;
				of << "\t\t\tRESULTS1" << endl;
				for(int i = 0; i < ex->currentSet; i++)
				{
					of << "\t\t\t\tresult1=" << ex->results1[i] << endl;
				}
				of << "\t\t\tENDRESULTS1" << endl;
				of << "\t\t\tRESULTS2" << endl;
				for(int i = 0; i < ex->currentSet; i++)
				{
					of << "\t\t\t\tresult2=" << ex->results2[i] << endl;
				}
				of << "\t\t\tENDRESULTS2" << endl;
				//of << "\t\t\tresults1Goals=" << ex->results1Goals << endl;
				//of << "\t\t\tresults2Goals=" << ex->results2Goals << endl;
				of << noskipws << "\t\t\tdescription=" << ex->description << endl;
				of << "\t\tENDEXERCISE" << endl;
			}
			of << "\tENDWORKOUT" << endl;
		}
		of << "ENDCURRROUTINE" << endl;
	}

	for(std::vector<routine>::iterator routine = completedRoutines.begin(); routine != completedRoutines.end(); ++routine) {
		of << "ROUTINE" << endl;
		of << "\tcurrWorkout=" << routine->currWorkout << endl;
		for(std::vector<workout>::iterator workout = routine->r.begin(); workout != routine->r.end(); ++workout) {
			of << "\tWORKOUT" << endl;
			of << "\t\tcurrExercise=" << workout->currExercise << endl;
			of << "\t\tdate=" << workout->date << endl;
			for(std::vector<exercise>::iterator ex = workout->w.begin(); ex != workout->w.end(); ++ex)
			{
				of << "\t\tEXERCISE" << endl;
				of << noskipws << "\t\t\tname=" << ex->name << endl;
				of << "\t\t\tcardio=" << ex->cardio << endl;
				of << "\t\t\tcurrentSet=" << ex->currentSet << endl;
				of << "\t\t\tRESULTS1" << endl;
				for(int i = 0; i < ex->currentSet; i++)
				{
					of << "\t\t\t\tresult1=" << ex->results1[i] << endl;
				}
				of << "\t\t\tENDRESULTS1" << endl;
				of << "\t\t\tRESULTS2" << endl;
				for(int i = 0; i < ex->currentSet; i++)
				{
					of << "\t\t\t\tresult2=" << ex->results2[i] << endl;
				}
				of << "\t\t\tENDRESULTS2" << endl;
				//of << "\t\t\tresults1Goals=" << ex->results1Goals << endl;
				//of << "\t\t\tresults2Goals=" << ex->results2Goals << endl;
				of << "\t\t\tdescription=" << ex->description << endl;
				of << "\t\tENDEXERCISE" << endl;
			}
			of << "\tENDWORKOUT" << endl;
		}
		of << "ENDROUTINE" << endl;
	}

	of.close();
}

void user::load()
{
	this->ClearUser();
	string fname;
	ifstream file;
	fname = GetInstance()->GetUsername();
	//file.open(GetInstance()->GetUsername());
	file.open(fname);
	string line,key,value;
	while(file)
	{
		getline(file, line);
		if(line == "")
		{
			continue;
		}
		line = line.substr(line.find_first_not_of(" \t\r\n"));
		key = line.substr(0,line.find("="));
		value = line.substr(line.find("=")+1);
		if(strcmp(key.c_str(), "username") == 0)
		{
			SetUsername(value);
		}
		if(strcmp(key.c_str(), "password") == 0)
		{
			SetPassword(value);
		}
		if(strcmp(key.c_str(), "age") == 0)
		{
			SetAge(atoi(value.c_str()));
		}
		if(strcmp(key.c_str(), "initWeight") == 0)
		{
			SetInitWeight(atoi(value.c_str()));
		}
		if(strcmp(key.c_str(), "height_ft") == 0)
		{
			this->height.ft = atoi(value.c_str());
		}
		if(strcmp(key.c_str(), "height_in") == 0)
		{
			this->height.in = atoi(value.c_str());
		}
		if(strcmp(key.c_str(), "fitLevel") == 0)
		{
			SetFitLevel(atoi(value.c_str()));
		}
		if(strcmp(key.c_str(), "goals") == 0)
		{
			SetGoals(atoi(value.c_str()));
		}
		if(strcmp(key.c_str(), "bmi") == 0)
		{
			bmi = atof(value.c_str());
		}
		if(strcmp(key.c_str(), "intensity") == 0)
		{
			intensity = atof(value.c_str());
		}
		if(strcmp(key.c_str(), "CURRROUTINE") == 0)
		{
			while(strcmp(key.c_str(), "ENDCURRROUTINE") != 0)
			ParseRoutine(file, currRoutine, line, key, value);
		}
		if(strcmp(key.c_str(), "ROUTINE") == 0)
		{
			routine temp;
			while(strcmp(key.c_str(), "ENDROUTINE") != 0)
			ParseRoutine(file, temp, line, key, value);
			completedRoutines.push_back(temp);
		}
	}
	file.close();
}

void user::CalcBMI()
{
	bmi = ((double)weightz[0]/(double)((height.ft*12+height.in)*(height.ft*12+height.in)))*703.0;
}

double user::CalcIntensity()
{
	CalcBMI();
	int a, b; // will hold determined values: 2 is high, 0 is low
	if(bmi < 18.5)//underweight
		a = 1;
	else if(bmi > 18.5 && bmi < 24.9)//healthy
		a = 2;
	else if(bmi > 25 && bmi < 29.9)//overweight
		a = 1;
	else//obese
		a = 0;
		
	if(age >= 18 && age < 30)//rather young
		b = 2;
	else if(age >= 30 && age < 50)//middle age
		b = 1;
	else//prehistoric
		b = 0;
	
	//adds age and BMI and multiplies them by half of the percieved fitness
	intensity = (a + b + (0.5 * fitLevel))/5;
	
	if(intensity > 0 && intensity < 0.5)//low intensity
		intensity = 0;
	else if(intensity > 0.5 && intensity < 0.8)//mid intensity
		intensity = 1;
	else//high intensity
		intensity = 2;
		
	return intensity;
}

string user::AssignRoutine()
{
	intensity = CalcIntensity();
	if(goals == 0)
	{
		if(intensity == 0.00)
		{
			return "00.ROUTINE";
		}
		if(intensity == 1.00)
		{
			return "01.ROUTINE";
		}
		if(intensity == 2.00)
		{
			return "02.ROUTINE";
		}
	}
	if(goals == 1)
	{
		if(intensity == 0.00)
		{
			return "10.ROUTINE";
		}
		if(intensity == 1.00)
		{
			return "11.ROUTINE";
		}
		if(intensity == 2.00)
		{
			return "12.ROUTINE";
		}
	}
	return "";
}

int user::getNumWorkouts()
{
	ifstream in;
	in.open(userName);
	if(!in)
	{
		return 0;
	}
	int numWorkouts = 0;
	bool trigger = false;
	string line;
	while(!in.eof() && (trigger == false))
	{
		getline(in, line);
		if(line == "")
			continue;
		line = line.substr(line.find_first_not_of(" \t\r\n"));
		if(line.find("result1=") != string::npos)
		{
			trigger = true;
		}
	}
	in.close();
	in.open(userName);
	while(!in.eof())
	{
		getline(in, line);
		if(line == "")
		{
			continue;
		}
		line = line.substr(line.find_first_not_of(" \t\r\n"));
		if(strcmp(line.c_str(), "WORKOUT") == 0)
		{
			numWorkouts++;
		}
	}
	in.close();
	if(trigger == 1)
	{
		return (numWorkouts / 2);
	}
	return numWorkouts;
}

void user::LoadNewRoutine(string filename)
{
	
	ifstream file;
	file.open(filename);
	if(!file)return;
	string line,key,value;
	this->currRoutine.r.clear();
	getline(file, line);
	while(line == "")
		getline(file, line);
	line = line.substr(line.find_first_not_of(" \t\r\n"));
	key = line.substr(0,line.find("="));
	value = line.substr(line.find("=")+1);
	currRoutine.currWorkout = 0;
	if(strcmp(key.c_str(), "ROUTINE") == 0)
		{
			while(strcmp(key.c_str(), "ENDROUTINE") != 0)
			ParseRoutine(file, currRoutine, line, key, value);
		}

}

string user::GetExercise(int pos)
{
	if(pos >= (int)this->currRoutine.r[this->currRoutine.currWorkout].w.size())
	{
		return "";
	}
	else
	{
		return this->currRoutine.r[this->currRoutine.currWorkout].w[pos].name;
	}
}

string user::GetDescription(int pos)
{
	if(pos >= (int)this->currRoutine.r[this->currRoutine.currWorkout].w.size())
	{
		return "";
	}
	else
	{
		return this->currRoutine.r[this->currRoutine.currWorkout].w[pos].description;
	}
}

int user::GetNumExercises(int routineIndex)
{
	return this->currRoutine.r[routineIndex].w.size();
}

int user::GetCurrRoutineCardio()
{
	//This will return the cardio boolean for the current exercise in the current workout
	return currRoutine.r[this->currRoutine.currWorkout].w[currRoutine.r[this->currRoutine.currWorkout].currExercise].cardio;
}

void user::PushBackResults(int currentEx, int val1, int val2)
{
	int currworkout = currRoutine.r[this->currRoutine.currWorkout].currExercise;
	currRoutine.r[this->currRoutine.currWorkout].w[currentEx].results1.push_back(val1);
	currRoutine.r[this->currRoutine.currWorkout].w[currentEx].results2.push_back(val2);
	currRoutine.r[this->currRoutine.currWorkout].w[currentEx].currentSet += 1;
}

void user::ClearUser()
{
	age = 0;
	goals = 0; 
	fitLevel = 0;
	height.ft = 0;
	height.in = 0;
	bmi = 0;
	intensity = 0;
	currRoutine.r.clear();
	completedRoutines.clear();
}

void ParseRoutine(ifstream &file, routine &tempRoutine, string &line, string &key, string &value)
{;
	getline(file, line); // get one line from the user file
	if(line == "") // if the line is blank, return. There's nothing to be done.
		return;
	line = line.substr(line.find_first_not_of(" \t\r\n")); // the line may have some tabs and spaces before the key=value pair for formatting reasons. Trim them off before anything else is done.
	key = line.substr(0,line.find("=")); // parse the key out of the remaining line
	value = line.substr(line.find("=")+1); // parse the value out of the remaining line

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Compare the key with keywords used for saving the file																																	 //
	if(strcmp(key.c_str(), "currWorkout") == 0) // currWorkout is the key used to store the value of currWorkout in a routine																	 //
	{																																															 //
		tempRoutine.currWorkout = atoi(value.c_str()); // convert the string to an int and save it to the routine																				 //
	}																																															 //
	if(strcmp(key.c_str(), "WORKOUT") == 0) // WORKOUT tags indicate that the next set of lines will be used to store one workout of this routine												 //
	{																																															 //
		workout tempWorkout; // used for storing the workout data as we continue to parse through the file																						 //
		while(strcmp(key.c_str(), "ENDWORKOUT") != 0) // parse the file until you hit an ENDWORKOUT tag. This means that all of the																 //
													  // data for the current workout has been parsed and you can then move on to the next one.													 //
		{																																														 //
			ParseWorkout(file, tempWorkout, line, key, value);																																	 //
		}																																														 //
		tempRoutine.r.push_back(tempWorkout); // now that the workout is parsed, push it onto the vector of workouts																			 //
	}																																															 //
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

void ParseWorkout(ifstream &file, workout &tempWorkout, string &line, string &key, string &value)
{
	getline(file, line); // get one line from the user file
	if(line == "") // if the line is blank, return. There's nothing to be done.
		return;
	line = line.substr(line.find_first_not_of(" \t\r\n")); // the line may have some tabs and spaces before the key=value pair for formatting reasons. Trim them off before anything else is done.
	key = line.substr(0,line.find("=")); // parse the key out of the remaining line
	value = line.substr(line.find("=")+1); // parse the value out of the remaining line

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Compare the key with keywords used for saving the file																																	 //
	if(strcmp(key.c_str(), "currExercise") == 0) // currExercise is the key used to store the value of currExercise in a workout																 //
	{																																															 //
		tempWorkout.currExercise = atoi(value.c_str()); // convert the string to an int and save it to the routine																				 //
	}																																															 //
	if(strcmp(key.c_str(), "EXERCISE") == 0) // EXERCISE tags indicate that the next set of lines will be used to store one exercise of this workout											 //
	{																																															 //
		exercise tempExercise; // used for storing the exercise data as we continue to parse through the file																					 //
		while(strcmp(key.c_str(), "ENDEXERCISE") != 0) // parse the file until you hit an ENDEXERCISE tag. This means that all of the															 //
													   // data for the current exercise has been parsed and you can then move on to the next one.												 //
		{																																														 //
			ParseExercise(file, tempExercise, line, key, value);																																 //
		}																																														 //
		tempWorkout.w.push_back(tempExercise); // now that the exercise is parsed, push it onto the vector of exercises																			 //
	}																																															 //
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

void ParseExercise(ifstream &file, exercise &tempExercise, string &line, string &key, string &value)
{
	getline(file, line); // get one line from the user file
	if(line == "") // if the line is blank, return. There's nothing to be done.
		return;
	line = line.substr(line.find_first_not_of(" \t\r\n")); // the line may have some tabs and spaces before the key=value pair for formatting reasons. Trim them off before anything else is done.
	key = line.substr(0,line.find("=")); // parse the key out of the remaining line
	value = line.substr(line.find("=")+1); // parse the value out of the remaining line

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Compare the key with keywords used for saving the file																																	 //
	if(strcmp(key.c_str(), "name") == 0)
	{
		tempExercise.name = value;
	}
	if(strcmp(key.c_str(), "cardio") == 0)
	{
		if(strcmp(value.c_str(),"1") == 0)
		{
			tempExercise.cardio = true;
		}
		else/* if(strcmp(value.c_str(), "false"))*/
		{
			tempExercise.cardio = false;
		}
	}
	if(strcmp(key.c_str(), "currentSet") == 0)
	{
		tempExercise.currentSet = atoi(value.c_str());
	}
	if(strcmp(key.c_str(), "result1") == 0)
	{
		tempExercise.results1.push_back(atoi(value.c_str()));
	}
	if(strcmp(key.c_str(), "result2") == 0)
	{
		tempExercise.results2.push_back(atoi(value.c_str()));
	}
	/*if(strcmp(key.c_str(), "results1Goal") == 0)
	{
	tempExercise.results1Goals = atoi(value.c_str());
	}
	if(strcmp(key.c_str(), "results2Goal") == 0)
	{
	tempExercise.results2Goals = atoi(value.c_str());
	}*/
	if(strcmp(key.c_str(), "description") == 0)
	{
		tempExercise.description = value;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}