#pragma once

namespace CEN4010_PROJECT3222012 {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;
	using namespace System::IO;
	#include <time.h>
#include <stdio.h>

	/// <summary>
	/// Summary for enterResults
	/// </summary>
	public ref class enterResults : public System::Windows::Forms::Form
	{
	public:
		static bool b1Click = false;
		static bool b2Click = false;
		static bool b3Click = false;
		static int currEx = 0;
		static int currRout = user::GetInstance()->GetCurrRoutine().currWorkout;
	private: System::Windows::Forms::Label^  E_Submit;
	public: 

	public: 
		static int curr = 0;
	private: System::Windows::Forms::Label^  label11;
	public: 
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
			 static int c = 1;
	public:
		enterResults(void)
		{
			currEx = 0;
			currRout = user::GetInstance()->GetCurrRoutine().currWorkout;
			InitializeComponent();
			pullInfo();
		}

	private:
		void pullInfo()
		{
			this->label2->Text = L"";
			this->label3->Text = L"";
			this->label4->Text = L"";
			this->label5->Text = L"";
			this->label6->Text = L"";
			this->label7->Text = L"";
			this->desc1->Text = L"";
			this->desc2->Text = L"";
			this->desc3->Text = L"";
			this->label12->Text = L"1";
			this->label13->Text = L"1";
			this->label14->Text = L"1";

			int temp = user::GetInstance()->GetNumExercises(currRout);

			if((user::GetInstance()->GetNumExercises(currRout) - currEx) >= 3)
			{
				this->label2->Text = Convert::ToString(currEx + 1) + ".";
				this->label3->Text = Convert::ToString(currEx + 2) + ".";
				this->label4->Text = Convert::ToString(currEx + 3) + ".";
				this->label5->Text = gcnew String(user::GetInstance()->GetExercise(currEx).c_str());
				this->label6->Text = gcnew String(user::GetInstance()->GetExercise(currEx + 1).c_str());
				this->label7->Text = gcnew String(user::GetInstance()->GetExercise(currEx + 2).c_str());
				this->desc1->Text = gcnew String(user::GetInstance()->GetDescription(currEx).c_str());
				this->desc2->Text = gcnew String(user::GetInstance()->GetDescription(currEx + 1).c_str());
				this->desc3->Text = gcnew String(user::GetInstance()->GetDescription(currEx + 2).c_str());
				curr = 3;
			}
			if((user::GetInstance()->GetNumExercises(currRout) - currEx) == 2)
			{
				this->label2->Text = Convert::ToString(currEx + 1) + ".";
				this->label3->Text = Convert::ToString(currEx + 2) + ".";
				this->label5->Text = gcnew String(user::GetInstance()->GetExercise(currEx).c_str());
				this->label6->Text = gcnew String(user::GetInstance()->GetExercise(currEx + 1).c_str());
				this->desc1->Text = gcnew String(user::GetInstance()->GetDescription(currEx).c_str());
				this->desc2->Text = gcnew String(user::GetInstance()->GetDescription(currEx + 1).c_str());
				this->label4->Visible = false;
				this->textBox5->Visible = false;
				this->textBox6->Visible = false;
				this->button3->Visible = false;
				this->label14->Visible = false;
				curr = 2;
			}
			if((user::GetInstance()->GetNumExercises(currRout) - currEx) == 1)
			{
				this->label2->Text = Convert::ToString(currEx + 1) + ".";
				this->label5->Text = gcnew String(user::GetInstance()->GetExercise(currEx).c_str());
				this->desc1->Text = gcnew String(user::GetInstance()->GetDescription(currEx).c_str());
				this->label3->Visible = false;
				this->label4->Visible = false;
				this->textBox3->Visible = false;
				this->textBox4->Visible = false;
				this->textBox5->Visible = false;
				this->textBox6->Visible = false;
				this->button2->Visible = false;
				this->button3->Visible = false;
				this->label13->Visible = false;
				this->label14->Visible = false;
				curr = 1;
			}
			if((user::GetInstance()->GetNumExercises(currRout) - currEx) <= 3)
			{
				this->button4->Text = L"Done";
			}
			c = user::GetInstance()->GetCurrRoutineCardio();
			if(c == 0)
			{
				this->label9->Text = L"Reps:";
				this->label10->Text = L"Weight:";
			}
			else
			{
				this->label9->Text = L"Distance:";
				this->label10->Text = L"Time:";
			}
		}
	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~enterResults()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Label^  desc1;
	private: System::Windows::Forms::Label^  desc2;
	private: System::Windows::Forms::Label^  desc3;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->desc1 = (gcnew System::Windows::Forms::Label());
			this->desc2 = (gcnew System::Windows::Forms::Label());
			this->desc3 = (gcnew System::Windows::Forms::Label());
			this->E_Submit = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(109, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Your Current Routine:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 70);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 13);
			this->label2->TabIndex = 1;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(12, 190);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(0, 13);
			this->label3->TabIndex = 2;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(12, 310);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(0, 13);
			this->label4->TabIndex = 3;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(34, 70);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(0, 13);
			this->label5->TabIndex = 4;
			this->label5->Click += gcnew System::EventHandler(this, &enterResults::label5_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(34, 190);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(0, 13);
			this->label6->TabIndex = 5;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(34, 310);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(0, 13);
			this->label7->TabIndex = 6;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(381, 9);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(70, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"Your Results:";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(358, 41);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(0, 13);
			this->label9->TabIndex = 8;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(445, 41);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(0, 13);
			this->label10->TabIndex = 9;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(343, 70);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(65, 20);
			this->textBox1->TabIndex = 10;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(437, 70);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(65, 20);
			this->textBox2->TabIndex = 11;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(343, 183);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(65, 20);
			this->textBox3->TabIndex = 12;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(437, 183);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(65, 20);
			this->textBox4->TabIndex = 13;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(343, 303);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(65, 20);
			this->textBox5->TabIndex = 14;
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(437, 303);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(65, 20);
			this->textBox6->TabIndex = 15;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(523, 67);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(89, 23);
			this->button1->TabIndex = 16;
			this->button1->Text = L"Submit Results";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &enterResults::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(523, 180);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(89, 23);
			this->button2->TabIndex = 17;
			this->button2->Text = L"Submit Results";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &enterResults::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(523, 300);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(89, 23);
			this->button3->TabIndex = 18;
			this->button3->Text = L"Submit Results";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &enterResults::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(536, 407);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 19;
			this->button4->Text = L"Next";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &enterResults::button4_Click);
			// 
			// desc1
			// 
			this->desc1->AutoSize = true;
			this->desc1->Location = System::Drawing::Point(37, 95);
			this->desc1->Name = L"desc1";
			this->desc1->Size = System::Drawing::Size(0, 13);
			this->desc1->TabIndex = 20;
			// 
			// desc2
			// 
			this->desc2->AutoSize = true;
			this->desc2->Location = System::Drawing::Point(37, 211);
			this->desc2->Name = L"desc2";
			this->desc2->Size = System::Drawing::Size(0, 13);
			this->desc2->TabIndex = 21;
			// 
			// desc3
			// 
			this->desc3->AutoSize = true;
			this->desc3->Location = System::Drawing::Point(37, 330);
			this->desc3->Name = L"desc3";
			this->desc3->Size = System::Drawing::Size(0, 13);
			this->desc3->TabIndex = 22;
			// 
			// E_Submit
			// 
			this->E_Submit->AutoSize = true;
			this->E_Submit->ForeColor = System::Drawing::Color::Red;
			this->E_Submit->Location = System::Drawing::Point(343, 416);
			this->E_Submit->Name = L"E_Submit";
			this->E_Submit->Size = System::Drawing::Size(183, 13);
			this->E_Submit->TabIndex = 23;
			this->E_Submit->Text = L"Submit your results before continuing.";
			this->E_Submit->Visible = false;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(194, 9);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(63, 13);
			this->label11->TabIndex = 24;
			this->label11->Text = L"Current Set:";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(197, 70);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(13, 13);
			this->label12->TabIndex = 25;
			this->label12->Text = L"1";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(197, 190);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(13, 13);
			this->label13->TabIndex = 26;
			this->label13->Text = L"1";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(197, 310);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(13, 13);
			this->label14->TabIndex = 27;
			this->label14->Text = L"1";
			// 
			// enterResults
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(624, 442);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->E_Submit);
			this->Controls->Add(this->desc3);
			this->Controls->Add(this->desc2);
			this->Controls->Add(this->desc1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"enterResults";
			this->Text = L"Enter Results";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 b1Click = true;
			 //this  code can swallow a load 8=====D~~~~~~ >�)
			 user::GetInstance()->PushBackResults(currEx, Convert::ToInt32(this->textBox1->Text), Convert::ToInt32(this->textBox2->Text));
	/*		 MessageBox::Show(Convert::ToString(user::GetInstance()->GetCurrRoutine().r[0].w[currEx].results1[1]));
			 MessageBox::Show(Convert::ToString(user::GetInstance()->GetCurrRoutine().r[0].w[currEx].results2[1]));*/
			 this->textBox1->Text = L"";
			 this->textBox2->Text = L"";
			 this->label12->Text = Convert::ToString(user::GetInstance()->GetCurrRoutine().r[currRout].w[currEx].currentSet + 1);
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 b2Click = true;
			 user::GetInstance()->PushBackResults((currEx + 1), Convert::ToInt32(this->textBox3->Text), Convert::ToInt32(this->textBox4->Text));
			 this->textBox3->Text = L"";
			 this->textBox4->Text = L"";
			 this->label13->Text = Convert::ToString(user::GetInstance()->GetCurrRoutine().r[currRout].w[currEx + 1].currentSet + 1);
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 b3Click = true;
			 user::GetInstance()->PushBackResults((currEx + 2), Convert::ToInt32(this->textBox5->Text), Convert::ToInt32(this->textBox6->Text));
			 this->textBox5->Text = L"";
			 this->textBox6->Text = L"";
			 this->label14->Text = Convert::ToString(user::GetInstance()->GetCurrRoutine().r[currRout].w[currEx + 2].currentSet + 1);
		 }
private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(this->button4->Text == L"Done")
			 {
				 if((this->button1->Visible == true) && b1Click == false)
				 {
					 E_Submit->Visible = true;
				 }
				 else if((this->button2->Visible == true) && b2Click == false)
				 {
					 E_Submit->Visible = true;
				 }
				 else if((this->button3->Visible == true) && b3Click == false)
				 {
					 E_Submit->Visible = true;
				 }
				 else
				 {
					user::GetInstance()->SetDate(misc::StringToString(DateTime::Now.ToShortDateString()));
					user::GetInstance()->IncrementWorkout();
					if(user::GetInstance()->GetCurrRoutine().currWorkout >= (int)user::GetInstance()->GetCurrRoutine().r.size())
					{
						user::GetInstance()->IncrementRoutine();
						user::GetInstance()->LoadNewRoutine(user::GetInstance()->AssignRoutine());
					}
					user::GetInstance()->save();
					this->Close();
				 }
			 }
			 if(this->button4->Text == L"Next")
			 {
				 currEx += 3;
				 b1Click = false;
				 b2Click = false;
				 b3Click = false;
				 this->textBox1->Visible = true;
				 this->textBox2->Visible = true;
				 this->textBox3->Visible = true;
				 this->textBox4->Visible = true;
				 this->textBox5->Visible = true;
				 this->textBox6->Visible = true;
				 pullInfo();
			 }

		 }
};
}