#include "user.h"
#include "enterResults.h"
#include "previousWorkouts.h"
#pragma once

namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for WelcomeScreen
	/// </summary>
	public ref class WelcomeScreen : public System::Windows::Forms::Form
	{
	public:
		WelcomeScreen(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			this->label4->Text = gcnew String(user::GetInstance()->GetUsername().c_str());
			this->label5->Text = gcnew String(Convert::ToString(user::GetInstance()->GetWeight()));
			this->label6->Text = gcnew String(Convert::ToString(user::GetInstance()->GetBMI()));
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~WelcomeScreen()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(30, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(52, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Welcome";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(30, 52);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(41, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Weight";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(30, 86);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(26, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"BMI";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(88, 15);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(0, 13);
			this->label4->TabIndex = 3;
			this->label4->Click += gcnew System::EventHandler(this, &WelcomeScreen::label4_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(77, 52);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(0, 13);
			this->label5->TabIndex = 4;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(62, 86);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(0, 13);
			this->label6->TabIndex = 5;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(91, 258);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(130, 65);
			this->button1->TabIndex = 6;
			this->button1->Text = L"Current Workout";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &WelcomeScreen::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(337, 258);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(130, 65);
			this->button2->TabIndex = 7;
			this->button2->Text = L"Previous Workouts";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &WelcomeScreen::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(464, 15);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(89, 28);
			this->button3->TabIndex = 8;
			this->button3->Text = L"Logout";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &WelcomeScreen::button3_Click);
			// 
			// WelcomeScreen
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(565, 360);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"WelcomeScreen";
			this->Text = L"Welcome to Fitness Wizard!";
			this->Load += gcnew System::EventHandler(this, &WelcomeScreen::WelcomeScreen_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label4_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
				 previousWorkouts ^ form666 = gcnew previousWorkouts();
				 form666->ShowDialog();
			 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 enterResults ^ form3 = gcnew enterResults();
			 form3->ShowDialog();
		 }
private: System::Void WelcomeScreen_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 user::GetInstance()->ClearUser();
			 this->Close();
		 }
};
}
