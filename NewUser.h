#pragma once

#include "misc.h"
#include "FitWizard1.h"

namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for NewUser
	/// </summary>
	public ref class NewUser : public System::Windows::Forms::Form
	{
	public:
		NewUser(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			this->txtUsername->Text = gcnew String(user::GetInstance()->GetUsername().c_str());
			this->txtPassword->Text = gcnew String(user::GetInstance()->GetPassword().c_str());
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~NewUser()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::TextBox^  txtUsername;
	private: System::Windows::Forms::TextBox^  txtPassword;
	private: System::Windows::Forms::TextBox^  txtConfirmPassword;
	private: System::Windows::Forms::TextBox^  txtAge;
	private: System::Windows::Forms::TextBox^  txtWeight;
	private: System::Windows::Forms::TextBox^  txtHeightFt;
	private: System::Windows::Forms::TextBox^  txtHeightIn;








	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::RadioButton^  radioButton1;
	private: System::Windows::Forms::RadioButton^  radioButton2;
	private: System::Windows::Forms::RadioButton^  radioButton3;
	private: System::Windows::Forms::RadioButton^  radioButton4;
	private: System::Windows::Forms::RadioButton^  radioButton5;
	private: System::Windows::Forms::GroupBox^  rgFitLevel;
	private: System::Windows::Forms::GroupBox^  rgFitGoal;


	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Button^  btnFitWizard;
	private: System::Windows::Forms::Button^  btnSubmit;
	private: System::Windows::Forms::Button^  btnCancel;
	private: System::Windows::Forms::Label^  E_user;
	private: System::Windows::Forms::Label^  E_Password;
	private: System::Windows::Forms::Label^  E_ConfirmPassword;
	private: System::Windows::Forms::Label^  E_age;
	private: System::Windows::Forms::Label^  E_weight;
	private: System::Windows::Forms::Label^  E_height;
	private: System::Windows::Forms::Label^  E_flevel;
	private: System::Windows::Forms::Label^  E_fgoal;




	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->txtUsername = (gcnew System::Windows::Forms::TextBox());
			this->txtPassword = (gcnew System::Windows::Forms::TextBox());
			this->txtConfirmPassword = (gcnew System::Windows::Forms::TextBox());
			this->txtAge = (gcnew System::Windows::Forms::TextBox());
			this->txtWeight = (gcnew System::Windows::Forms::TextBox());
			this->txtHeightFt = (gcnew System::Windows::Forms::TextBox());
			this->txtHeightIn = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->radioButton1 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton2 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton3 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton4 = (gcnew System::Windows::Forms::RadioButton());
			this->radioButton5 = (gcnew System::Windows::Forms::RadioButton());
			this->rgFitLevel = (gcnew System::Windows::Forms::GroupBox());
			this->rgFitGoal = (gcnew System::Windows::Forms::GroupBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->btnFitWizard = (gcnew System::Windows::Forms::Button());
			this->btnSubmit = (gcnew System::Windows::Forms::Button());
			this->btnCancel = (gcnew System::Windows::Forms::Button());
			this->E_user = (gcnew System::Windows::Forms::Label());
			this->E_Password = (gcnew System::Windows::Forms::Label());
			this->E_ConfirmPassword = (gcnew System::Windows::Forms::Label());
			this->E_age = (gcnew System::Windows::Forms::Label());
			this->E_weight = (gcnew System::Windows::Forms::Label());
			this->E_height = (gcnew System::Windows::Forms::Label());
			this->E_flevel = (gcnew System::Windows::Forms::Label());
			this->E_fgoal = (gcnew System::Windows::Forms::Label());
			this->rgFitLevel->SuspendLayout();
			this->rgFitGoal->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Username:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(13, 36);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(56, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Password:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(13, 66);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(94, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Confirm Password:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(13, 107);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(28, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"age:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(12, 140);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(41, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"weight:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(12, 174);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(39, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"height:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(13, 223);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(65, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"fitness level:";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(13, 272);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(63, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"fitness goal:";
			// 
			// txtUsername
			// 
			this->txtUsername->Location = System::Drawing::Point(155, 6);
			this->txtUsername->Name = L"txtUsername";
			this->txtUsername->Size = System::Drawing::Size(241, 20);
			this->txtUsername->TabIndex = 8;
			this->txtUsername->TextChanged += gcnew System::EventHandler(this, &NewUser::txtUsername_TextChanged);
			// 
			// txtPassword
			// 
			this->txtPassword->AcceptsReturn = true;
			this->txtPassword->Location = System::Drawing::Point(155, 33);
			this->txtPassword->Name = L"txtPassword";
			this->txtPassword->Size = System::Drawing::Size(241, 20);
			this->txtPassword->TabIndex = 9;
			this->txtPassword->UseSystemPasswordChar = true;
			// 
			// txtConfirmPassword
			// 
			this->txtConfirmPassword->Location = System::Drawing::Point(155, 59);
			this->txtConfirmPassword->Name = L"txtConfirmPassword";
			this->txtConfirmPassword->Size = System::Drawing::Size(241, 20);
			this->txtConfirmPassword->TabIndex = 10;
			this->txtConfirmPassword->UseSystemPasswordChar = true;
			// 
			// txtAge
			// 
			this->txtAge->Location = System::Drawing::Point(155, 104);
			this->txtAge->Name = L"txtAge";
			this->txtAge->Size = System::Drawing::Size(55, 20);
			this->txtAge->TabIndex = 11;
			this->txtAge->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &NewUser::OnlyNumbers);
			// 
			// txtWeight
			// 
			this->txtWeight->Location = System::Drawing::Point(155, 137);
			this->txtWeight->Name = L"txtWeight";
			this->txtWeight->Size = System::Drawing::Size(55, 20);
			this->txtWeight->TabIndex = 12;
			this->txtWeight->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &NewUser::OnlyNumbers);
			// 
			// txtHeightFt
			// 
			this->txtHeightFt->Location = System::Drawing::Point(155, 171);
			this->txtHeightFt->Name = L"txtHeightFt";
			this->txtHeightFt->Size = System::Drawing::Size(30, 20);
			this->txtHeightFt->TabIndex = 13;
			this->txtHeightFt->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &NewUser::OnlyNumbers);
			// 
			// txtHeightIn
			// 
			this->txtHeightIn->Location = System::Drawing::Point(210, 171);
			this->txtHeightIn->Name = L"txtHeightIn";
			this->txtHeightIn->Size = System::Drawing::Size(30, 20);
			this->txtHeightIn->TabIndex = 14;
			this->txtHeightIn->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &NewUser::OnlyNumbers);
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(216, 107);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(32, 13);
			this->label9->TabIndex = 15;
			this->label9->Text = L"years";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(216, 140);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(23, 13);
			this->label10->TabIndex = 16;
			this->label10->Text = L"lbs.";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(191, 174);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(9, 13);
			this->label11->TabIndex = 17;
			this->label11->Text = L"\'";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(246, 174);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(12, 13);
			this->label12->TabIndex = 18;
			this->label12->Text = L"\"";
			// 
			// radioButton1
			// 
			this->radioButton1->AutoSize = true;
			this->radioButton1->Location = System::Drawing::Point(17, 8);
			this->radioButton1->Name = L"radioButton1";
			this->radioButton1->Size = System::Drawing::Size(56, 17);
			this->radioButton1->TabIndex = 19;
			this->radioButton1->TabStop = true;
			this->radioButton1->Text = L"Not Fit";
			this->radioButton1->UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this->radioButton2->AutoSize = true;
			this->radioButton2->Location = System::Drawing::Point(82, 8);
			this->radioButton2->Name = L"radioButton2";
			this->radioButton2->Size = System::Drawing::Size(91, 17);
			this->radioButton2->TabIndex = 20;
			this->radioButton2->TabStop = true;
			this->radioButton2->Text = L"Moderately Fit";
			this->radioButton2->UseVisualStyleBackColor = true;
			// 
			// radioButton3
			// 
			this->radioButton3->AutoSize = true;
			this->radioButton3->Location = System::Drawing::Point(178, 8);
			this->radioButton3->Name = L"radioButton3";
			this->radioButton3->Size = System::Drawing::Size(84, 17);
			this->radioButton3->TabIndex = 21;
			this->radioButton3->TabStop = true;
			this->radioButton3->Text = L"Extremely Fit";
			this->radioButton3->UseVisualStyleBackColor = true;
			// 
			// radioButton4
			// 
			this->radioButton4->AutoSize = true;
			this->radioButton4->Location = System::Drawing::Point(11, 10);
			this->radioButton4->Name = L"radioButton4";
			this->radioButton4->Size = System::Drawing::Size(77, 17);
			this->radioButton4->TabIndex = 22;
			this->radioButton4->TabStop = true;
			this->radioButton4->Text = L"weight loss";
			this->radioButton4->UseVisualStyleBackColor = true;
			// 
			// radioButton5
			// 
			this->radioButton5->AutoSize = true;
			this->radioButton5->Location = System::Drawing::Point(94, 10);
			this->radioButton5->Name = L"radioButton5";
			this->radioButton5->Size = System::Drawing::Size(81, 17);
			this->radioButton5->TabIndex = 23;
			this->radioButton5->TabStop = true;
			this->radioButton5->Text = L"muscle gain";
			this->radioButton5->UseVisualStyleBackColor = true;
			// 
			// rgFitLevel
			// 
			this->rgFitLevel->Controls->Add(this->radioButton1);
			this->rgFitLevel->Controls->Add(this->radioButton2);
			this->rgFitLevel->Controls->Add(this->radioButton3);
			this->rgFitLevel->Location = System::Drawing::Point(155, 205);
			this->rgFitLevel->Name = L"rgFitLevel";
			this->rgFitLevel->Size = System::Drawing::Size(268, 31);
			this->rgFitLevel->TabIndex = 24;
			this->rgFitLevel->TabStop = false;
			this->rgFitLevel->Text = L" ";
			// 
			// rgFitGoal
			// 
			this->rgFitGoal->Controls->Add(this->radioButton4);
			this->rgFitGoal->Controls->Add(this->radioButton5);
			this->rgFitGoal->Location = System::Drawing::Point(155, 252);
			this->rgFitGoal->Name = L"rgFitGoal";
			this->rgFitGoal->Size = System::Drawing::Size(183, 33);
			this->rgFitGoal->TabIndex = 25;
			this->rgFitGoal->TabStop = false;
			this->rgFitGoal->Text = L" ";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(515, 178);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(206, 26);
			this->label13->TabIndex = 26;
			this->label13->Text = L"Don\'t know\? \r\nUse the fitness wizard to help you find out!";
			this->label13->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// btnFitWizard
			// 
			this->btnFitWizard->Location = System::Drawing::Point(557, 205);
			this->btnFitWizard->Name = L"btnFitWizard";
			this->btnFitWizard->Size = System::Drawing::Size(119, 32);
			this->btnFitWizard->TabIndex = 27;
			this->btnFitWizard->Text = L"Fitness Wizard";
			this->btnFitWizard->UseVisualStyleBackColor = true;
			this->btnFitWizard->Click += gcnew System::EventHandler(this, &NewUser::btnFitWizard_Click);
			// 
			// btnSubmit
			// 
			this->btnSubmit->Location = System::Drawing::Point(548, 339);
			this->btnSubmit->Name = L"btnSubmit";
			this->btnSubmit->Size = System::Drawing::Size(75, 23);
			this->btnSubmit->TabIndex = 28;
			this->btnSubmit->Text = L"Submit";
			this->btnSubmit->UseVisualStyleBackColor = true;
			this->btnSubmit->Click += gcnew System::EventHandler(this, &NewUser::btnSubmit_Click);
			// 
			// btnCancel
			// 
			this->btnCancel->Location = System::Drawing::Point(630, 338);
			this->btnCancel->Name = L"btnCancel";
			this->btnCancel->Size = System::Drawing::Size(75, 23);
			this->btnCancel->TabIndex = 29;
			this->btnCancel->Text = L"cancel";
			this->btnCancel->UseVisualStyleBackColor = true;
			this->btnCancel->Click += gcnew System::EventHandler(this, &NewUser::btnCancel_Click);
			// 
			// E_user
			// 
			this->E_user->AutoSize = true;
			this->E_user->ForeColor = System::Drawing::Color::Red;
			this->E_user->Location = System::Drawing::Point(403, 9);
			this->E_user->Name = L"E_user";
			this->E_user->Size = System::Drawing::Size(130, 13);
			this->E_user->TabIndex = 30;
			this->E_user->Text = L"*Please enter a Username";
			this->E_user->Visible = false;
			// 
			// E_Password
			// 
			this->E_Password->AutoSize = true;
			this->E_Password->ForeColor = System::Drawing::Color::Red;
			this->E_Password->Location = System::Drawing::Point(403, 40);
			this->E_Password->Name = L"E_Password";
			this->E_Password->Size = System::Drawing::Size(128, 13);
			this->E_Password->TabIndex = 31;
			this->E_Password->Text = L"*Please enter a Password";
			this->E_Password->Visible = false;
			// 
			// E_ConfirmPassword
			// 
			this->E_ConfirmPassword->AutoSize = true;
			this->E_ConfirmPassword->ForeColor = System::Drawing::Color::Red;
			this->E_ConfirmPassword->Location = System::Drawing::Point(403, 66);
			this->E_ConfirmPassword->Name = L"E_ConfirmPassword";
			this->E_ConfirmPassword->Size = System::Drawing::Size(152, 13);
			this->E_ConfirmPassword->TabIndex = 32;
			this->E_ConfirmPassword->Text = L"*Please confirm your Password";
			this->E_ConfirmPassword->Visible = false;
			// 
			// E_age
			// 
			this->E_age->AutoSize = true;
			this->E_age->ForeColor = System::Drawing::Color::Red;
			this->E_age->Location = System::Drawing::Point(267, 111);
			this->E_age->Name = L"E_age";
			this->E_age->Size = System::Drawing::Size(114, 13);
			this->E_age->TabIndex = 33;
			this->E_age->Text = L"*Please enter your age";
			this->E_age->Visible = false;
			// 
			// E_weight
			// 
			this->E_weight->AutoSize = true;
			this->E_weight->ForeColor = System::Drawing::Color::Red;
			this->E_weight->Location = System::Drawing::Point(267, 144);
			this->E_weight->Name = L"E_weight";
			this->E_weight->Size = System::Drawing::Size(127, 13);
			this->E_weight->TabIndex = 34;
			this->E_weight->Text = L"*Please enter your weight";
			this->E_weight->Visible = false;
			// 
			// E_height
			// 
			this->E_height->AutoSize = true;
			this->E_height->ForeColor = System::Drawing::Color::Red;
			this->E_height->Location = System::Drawing::Point(267, 178);
			this->E_height->Name = L"E_height";
			this->E_height->Size = System::Drawing::Size(125, 13);
			this->E_height->TabIndex = 35;
			this->E_height->Text = L"*Please enter your height";
			this->E_height->Visible = false;
			// 
			// E_flevel
			// 
			this->E_flevel->AutoSize = true;
			this->E_flevel->ForeColor = System::Drawing::Color::Red;
			this->E_flevel->Location = System::Drawing::Point(429, 217);
			this->E_flevel->Name = L"E_flevel";
			this->E_flevel->Size = System::Drawing::Size(126, 13);
			this->E_flevel->TabIndex = 36;
			this->E_flevel->Text = L"*No fitness level selected";
			this->E_flevel->Visible = false;
			// 
			// E_fgoal
			// 
			this->E_fgoal->AutoSize = true;
			this->E_fgoal->ForeColor = System::Drawing::Color::Red;
			this->E_fgoal->Location = System::Drawing::Point(344, 266);
			this->E_fgoal->Name = L"E_fgoal";
			this->E_fgoal->Size = System::Drawing::Size(124, 13);
			this->E_fgoal->TabIndex = 37;
			this->E_fgoal->Text = L"*No fitness goal selected";
			this->E_fgoal->Visible = false;
			// 
			// NewUser
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(720, 374);
			this->Controls->Add(this->E_fgoal);
			this->Controls->Add(this->E_flevel);
			this->Controls->Add(this->E_height);
			this->Controls->Add(this->E_weight);
			this->Controls->Add(this->E_age);
			this->Controls->Add(this->E_ConfirmPassword);
			this->Controls->Add(this->E_Password);
			this->Controls->Add(this->E_user);
			this->Controls->Add(this->btnCancel);
			this->Controls->Add(this->btnSubmit);
			this->Controls->Add(this->btnFitWizard);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->rgFitGoal);
			this->Controls->Add(this->rgFitLevel);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->txtHeightIn);
			this->Controls->Add(this->txtHeightFt);
			this->Controls->Add(this->txtWeight);
			this->Controls->Add(this->txtAge);
			this->Controls->Add(this->txtConfirmPassword);
			this->Controls->Add(this->txtPassword);
			this->Controls->Add(this->txtUsername);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"NewUser";
			this->Text = L"New User";
			this->Load += gcnew System::EventHandler(this, &NewUser::NewUser_Load);
			this->rgFitLevel->ResumeLayout(false);
			this->rgFitLevel->PerformLayout();
			this->rgFitGoal->ResumeLayout(false);
			this->rgFitGoal->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


private: System::Void OnlyNumbers(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 if (isdigit(e->KeyChar) || iscntrl(e->KeyChar)) {
				 e->Handled = false;
			 }
			 else {
				 e->Handled = true;
			 } 
		 }
private: System::Void btnCancel_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }


private: System::Void NewUser_Load(System::Object^  sender, System::EventArgs^  e) {

		 }
private: System::Void btnSubmit_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 bool E_occured = false;
			 E_user->Visible=false;
			 E_Password->Visible=false;
			 E_ConfirmPassword->Visible=false;
			 E_age->Visible=false;
			 E_weight->Visible=false;
			 E_height->Visible=false;
			 E_fgoal->Visible=false;
			 E_flevel->Visible=false;
	
			 if(txtUsername->Text=="")
			 {
				 E_user->Visible=true;
				 E_occured = true;
			 }
			 if(txtPassword->Text=="")
			 {
				 E_Password->Visible=true;
				 E_occured = true;
			 }
			 if(txtConfirmPassword->Text=="")
			 {
				  E_ConfirmPassword->Visible=true;
				  E_occured = true;
			 }
			 if(txtConfirmPassword->Text!=txtPassword->Text && txtPassword->Text!=" "&& txtConfirmPassword->Text!="")
			 { 
				MessageBox::Show("Your passwords don't match");
				txtConfirmPassword->Text="";
				txtPassword->Text="";
				E_occured = true;
			 }
			 if(txtAge->Text=="")
			 {
				  E_age->Visible=true;
				  E_occured = true;
			 }
			 if(txtWeight->Text=="")
			 {
				  E_weight->Visible=true;
				  E_occured = true;
			 }
			 if(txtHeightFt->Text=="" || txtHeightIn->Text=="")
			 {
				  E_height->Visible=true;
				  E_occured = true;
			 }
			 if(radioButton1->Checked==false && radioButton2->Checked==false && radioButton3->Checked==false)
			 {
				 E_flevel->Visible=true;
				 E_occured = true;
			 }
			 if(radioButton4->Checked==false && radioButton5->Checked==false)
			 {
				 E_fgoal->Visible=true;
				 E_occured = true;
			 }
			 if(!E_occured)
			 {
				 //get the username from the textbox and add it to the user object
				 user::GetInstance()->SetUsername(misc::TextboxToString(txtUsername));
				 //get the password from the textbox and add it to the user object
				 user::GetInstance()->SetPassword(misc::StringToString(misc::getMD5String(txtPassword->Text)));
				 user::GetInstance()->SetAge(int::Parse(txtAge->Text));
				 user::GetInstance()->SetInitWeight(int::Parse(txtWeight->Text));
				 user::GetInstance()->SetHeight(int::Parse(txtHeightFt->Text),int::Parse(txtHeightIn->Text));

				 if(radioButton1->Checked==true)
				 {
					 user::GetInstance()->SetFitLevel(0);
				 }else if(radioButton2->Checked==true)
				 {
					 user::GetInstance()->SetFitLevel(1);
				 }else if(radioButton3->Checked==true)
				 {
					 user::GetInstance()->SetFitLevel(2);
				 }
				 
				 if(radioButton4->Checked==true)
				 {
					 user::GetInstance()->SetGoals(1);
				 }else if(radioButton5->Checked==true)
				 {
					 user::GetInstance()->SetGoals(0);
				 }

				 //save it if everything is ok
				 string fname = "";
				 fname = user::GetInstance()->AssignRoutine();
				 user::GetInstance()->CalcIntensity();
				 user::GetInstance()->LoadNewRoutine(fname);
				 user::GetInstance()->save();
				 //then go either back to the login screen or straight into the main screen.
				 MessageBox::Show("Sucessfully created new user.");
				 this->Close();
			 }
		 }

private: System::Void btnFitWizard_Click(System::Object^  sender, System::EventArgs^  e) {
			 FitWizard ^form = gcnew FitWizard;
			 form->ShowDialog();

			 if(user::GetInstance()->GetFitLevel() == 1)
			 {
				 radioButton1->Checked=true;
			 }else if(user::GetInstance()->GetFitLevel() == 2)
			 {
				 radioButton2->Checked=true;
			 }else if(user::GetInstance()->GetFitLevel() == 3)
			 {
				 radioButton3->Checked=true;
			 }
		 }
private: System::Void txtUsername_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}
