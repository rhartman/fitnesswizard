#ifndef user_h
#define user_h
#include <iostream>
#include <string>
#include <vector>
#include "routine.h"

using namespace std;

class user
{
public:
	//Takes information from user (default constructor)
	user();
	static user* GetInstance();
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////// accessors //////////////////////////////////////////////
	//username																							//
	inline string GetUsername(){return GetInstance()->userName;}										//
	inline void SetUsername(string username){GetInstance()->userName = username;}						//
	//password																							//
	inline string GetPassword(){return GetInstance()->password;}										//
	inline void SetPassword(string passwd){GetInstance()->password = passwd;}							//
	//age																								//
	inline int GetAge(){return GetInstance()->age;}														//
	inline void SetAge(int newAge){GetInstance()->age = newAge;}										//
	//initWeight																						//
	inline int GetInitWeight(){return GetInstance()->weightz.front();}									//
	inline void SetInitWeight(int newInitWeight){GetInstance()->weightz[0] = newInitWeight;}			//
	//goals																								//
	inline int GetGoals(){return GetInstance()->goals;}													//
	inline void SetGoals(int newGoals){GetInstance()->goals = newGoals;}								//
	//fitLevel																							//
	inline int GetFitLevel(){return GetInstance()->fitLevel;}											//
	inline void SetFitLevel(int newFitLevel){GetInstance()->fitLevel = newFitLevel;}					//
	//height																							//
	inline int GetHeightFt(){return GetInstance()->height.ft;}											//
	inline int GetHeightIn(){return GetInstance()->height.in;}											//
	inline void SetHeight(int ft, int in){GetInstance()->height.ft = ft; GetInstance()->height.in = in;}//
	//currentWeight																						//
	inline int GetWeight(){return weightz.back();}														//
	//BMI																								//
	inline double GetBMI(){return bmi;}																	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	
	void save();
	void load();
	void LoadNewRoutine(string filename);
	string AssignRoutine();
	double CalcIntensity();
	string GetExercise(int pos);
	int GetNumExercises(int routineIndex);
	string GetDescription(int pos);
	int GetCurrRoutineCardio();
	inline routine GetCurrRoutine(){return currRoutine;}
	void PushBackResults(int currentEx, int val1, int val2);
	inline void IncrementWorkout() {currRoutine.currWorkout += 1;}
	inline void IncrementRoutine(){completedRoutines.push_back(currRoutine);}
	inline void SetDate(std::string date){currRoutine.r[currRoutine.currWorkout].date = date;}
	int getNumWorkouts();
	inline routine GetPrevRoutine() {return this->completedRoutines[0];}
	inline bool GetPrevRoutineCardio(int currWorkout) {return this->completedRoutines[0].r[currWorkout].w[0].cardio;}
	
	void ClearUser();
private:
	void CalcBMI();
	int age, goals, fitLevel;
	double intensity;
	vector<int> weightz;
	struct length
	{
		int ft;
		int in;
	}height;
	string userName, password;
	double bmi;
	static user *singleton;

	routine currRoutine;
	vector<routine> completedRoutines;
};
#endif