#ifndef MISC_H
#define MISC_H

#include "stdafx.h"

namespace misc
{
	using namespace System;
	using namespace System::Security::Cryptography;
	using namespace System::Text;
	std::string TextboxToString(System::Windows::Forms::TextBox ^ txt);
	String^ getMD5String(String^ inputString);
	std::string StringToString(System::String ^ txt);
}

#endif
