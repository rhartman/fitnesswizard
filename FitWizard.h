#pragma once

namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for FitWizard
	/// </summary>
	public ref class FitWizard : public System::Windows::Forms::Form
	{
	public:
		FitWizard(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FitWizard()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Button^  btnCalcFitLevel;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->btnCalcFitLevel = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(364, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(203, 17);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Welcome to the Fitness Wizard";
			this->label1->Click += gcnew System::EventHandler(this, &FitWizard::label1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(33, 73);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(495, 17);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Answer the following questions and my magic will determine your fitness level:";
			this->label2->Click += gcnew System::EventHandler(this, &FitWizard::label2_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(33, 124);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(322, 17);
			this->label3->TabIndex = 2;
			this->label3->Text = L"On a scale from 1-10, how often do you work out\?";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(399, 124);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(87, 22);
			this->textBox1->TabIndex = 4;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(33, 180);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(164, 17);
			this->label4->TabIndex = 5;
			this->label4->Text = L"On a scale from 1 to 10, ";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(399, 180);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(87, 22);
			this->textBox2->TabIndex = 6;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(33, 239);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(164, 17);
			this->label5->TabIndex = 7;
			this->label5->Text = L"On a scale from 1 to 10, ";
			this->label5->Click += gcnew System::EventHandler(this, &FitWizard::label5_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(399, 234);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(87, 22);
			this->textBox3->TabIndex = 8;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(33, 369);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(359, 17);
			this->label6->TabIndex = 9;
			this->label6->Text = L"According to my wizarding abilities, your fitness level is: ";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(399, 369);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 22);
			this->textBox4->TabIndex = 10;
			// 
			// btnCalcFitLevel
			// 
			this->btnCalcFitLevel->Location = System::Drawing::Point(174, 292);
			this->btnCalcFitLevel->Name = L"btnCalcFitLevel";
			this->btnCalcFitLevel->Size = System::Drawing::Size(160, 48);
			this->btnCalcFitLevel->TabIndex = 11;
			this->btnCalcFitLevel->Text = L"Submit";
			this->btnCalcFitLevel->UseVisualStyleBackColor = true;
			// 
			// FitWizard
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(945, 458);
			this->Controls->Add(this->btnCalcFitLevel);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"FitWizard";
			this->Text = L"Fitness Wizard";
			this->Load += gcnew System::EventHandler(this, &FitWizard::FitWizard_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void FitWizard_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
};
}
