#pragma once

namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for FitWizard
	/// </summary>
	public ref class FitWizard : public System::Windows::Forms::Form
	{
	public:
		FitWizard(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FitWizard()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::Button^  btnCalcFitLevel;
	private: System::Windows::Forms::Label^  txtResult;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private: int fitLevel;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->btnCalcFitLevel = (gcnew System::Windows::Forms::Button());
			this->txtResult = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(273, 7);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(154, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Welcome to the Fitness Wizard";
			this->label1->Click += gcnew System::EventHandler(this, &FitWizard::label1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(25, 59);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(370, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Answer the following questions and my magic will determine your fitness level:";
			this->label2->Click += gcnew System::EventHandler(this, &FitWizard::label2_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(25, 101);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(209, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"On a scale from 1-10, How active are you\?";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(299, 101);
			this->textBox1->Margin = System::Windows::Forms::Padding(2);
			this->textBox1->MaxLength = 2;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(66, 20);
			this->textBox1->TabIndex = 4;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FitWizard::OnlyNumbers);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(25, 146);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(232, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"On a scale from 1-10,  How healthy is your diet\?";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(299, 146);
			this->textBox2->Margin = System::Windows::Forms::Padding(2);
			this->textBox2->MaxLength = 2;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(66, 20);
			this->textBox2->TabIndex = 6;
			this->textBox2->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FitWizard::OnlyNumbers);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(25, 194);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(335, 26);
			this->label5->TabIndex = 7;
			this->label5->Text = L"On a scale from 1-10, How healthy are your habit\?\r\n(For example, if you drink and" 
				L" smoke you would enter a lower number)";
			this->label5->Click += gcnew System::EventHandler(this, &FitWizard::label5_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(299, 190);
			this->textBox3->Margin = System::Windows::Forms::Padding(2);
			this->textBox3->MaxLength = 2;
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(66, 20);
			this->textBox3->TabIndex = 8;
			this->textBox3->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FitWizard::OnlyNumbers);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(25, 300);
			this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(267, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"According to my wizarding abilities, your fitness level is: ";
			// 
			// btnCalcFitLevel
			// 
			this->btnCalcFitLevel->Location = System::Drawing::Point(130, 237);
			this->btnCalcFitLevel->Margin = System::Windows::Forms::Padding(2);
			this->btnCalcFitLevel->Name = L"btnCalcFitLevel";
			this->btnCalcFitLevel->Size = System::Drawing::Size(120, 39);
			this->btnCalcFitLevel->TabIndex = 11;
			this->btnCalcFitLevel->Text = L"Submit";
			this->btnCalcFitLevel->UseVisualStyleBackColor = true;
			this->btnCalcFitLevel->Click += gcnew System::EventHandler(this, &FitWizard::btnCalcFitLevel_Click);
			// 
			// txtResult
			// 
			this->txtResult->AutoSize = true;
			this->txtResult->Location = System::Drawing::Point(298, 300);
			this->txtResult->Name = L"txtResult";
			this->txtResult->Size = System::Drawing::Size(0, 13);
			this->txtResult->TabIndex = 12;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(379, 300);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 13;
			this->button1->Text = L"continue";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Visible = false;
			this->button1->Click += gcnew System::EventHandler(this, &FitWizard::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(566, 300);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 14;
			this->button2->Text = L"cancel";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &FitWizard::button2_Click);
			// 
			// FitWizard
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(709, 372);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->txtResult);
			this->Controls->Add(this->btnCalcFitLevel);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"FitWizard";
			this->Text = L"Fitness Wizard";
			this->Load += gcnew System::EventHandler(this, &FitWizard::FitWizard_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void FitWizard_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void label5_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
private: System::Void OnlyNumbers(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 if (isdigit(e->KeyChar) || iscntrl(e->KeyChar)) {
				 e->Handled = false;
			 }
			 else {
				 e->Handled = true;
			 } 
		 }
private: System::Void btnCalcFitLevel_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(int::Parse(textBox1->Text) > 10 || int::Parse(textBox2->Text) > 10 || int::Parse(textBox3->Text)>10)
			 {
				 MessageBox::Show("invalid input");
				 return;
			 }

			 int result=0;
			 result += int::Parse(textBox1->Text) + int::Parse(textBox2->Text) + int::Parse(textBox3->Text);
			 if(result <=10 && result >=0)
			 {
				 fitLevel = 1;
				 txtResult->Text = gcnew String("n00b");
			 }else if(result > 10 && result <= 20)
			 {
				 fitLevel = 2;
				 txtResult->Text = gcnew String("moderate");
			 }else if(result > 20)
			 {
				 fitLevel = 3;
				 txtResult->Text = gcnew String("1337");
			 }

			 button1->Visible = true;

		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			user::GetInstance()->SetFitLevel(fitLevel);
			this->Close();
		 }

};
}
