#include "stdafx.h"


	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

std::string misc::TextboxToString(System::Windows::Forms::TextBox ^ txt)
{
	return StringToString(txt->Text);
}

String^ misc::getMD5String(String^ inputString)
{
	inputString = L"BillCosby'sPuddingPops" + inputString;
	array<Byte>^ byteArray = Encoding::ASCII->GetBytes(inputString);
	MD5CryptoServiceProvider^ md5provider = gcnew MD5CryptoServiceProvider();
	array<Byte>^ byteArrayHash = md5provider->ComputeHash(byteArray);
	return BitConverter::ToString(byteArrayHash);
}
std::string misc::StringToString(System::String ^ txt)
{
	//start google magic to convert textbox text to std::string
	std::string temp;
	using namespace Runtime::InteropServices;
	const char* chars = 
		(const char*)(Marshal::StringToHGlobalAnsi(txt)).ToPointer();
	temp = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
	//end google magic
	return temp;
}