#pragma once

namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for previousWorkouts
	/// </summary>
	public ref class previousWorkouts : public System::Windows::Forms::Form
	{
	public:
		static int workoutNumber = 0;
		static int totalWorkouts = 0;
		previousWorkouts(void)
		{
			workoutNumber = 0;
			InitializeComponent();
			hideLabels();
			displayData(workoutNumber);
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~previousWorkouts()
		{
			if (components)
			{
				delete components;
			}
		}
	private:
		void hideLabels()
		{
			this->label3->Visible = false;
			this->label4->Visible = false;
			this->label5->Visible = false;
			this->label6->Visible = false;
			this->label7->Visible = false;
			this->label8->Visible = false;
			this->label9->Visible = false;
			this->label10->Visible = false;
			this->label11->Visible = false;
			this->label12->Visible = false;
			this->label13->Visible = false;
			this->label14->Visible = false;
			this->label15->Visible = false;
			this->label16->Visible = false;
			this->label17->Visible = false;
			this->label18->Visible = false;
			this->label19->Visible = false;
			this->label20->Visible = false;
			this->label21->Visible = false;
			this->label22->Visible = false;
			this->label23->Visible = false;
			this->label24->Visible = false;
			this->label25->Visible = false;
			this->label26->Visible = false;
			this->label27->Visible = false;
			this->label28->Visible = false;
			this->label29->Visible = false;
			this->label30->Visible = false;
			this->label31->Visible = false;
			this->label32->Visible = false;
			this->label33->Visible = false;
			this->label34->Visible = false;
			this->label35->Visible = false;
			this->label36->Visible = false;
			this->label37->Visible = false;
			this->label38->Visible = false;
			this->label39->Visible = false;
			this->label40->Visible = false;
			this->label41->Visible = false;
			this->label42->Visible = false;
			this->label43->Visible = false;
			this->label44->Visible = false;
			this->label45->Visible = false;
			this->label46->Visible = false;
		}
		void displayData(int workoutNum)
		{
			if(workoutNumber > totalWorkouts)
				 {
					 this->button1->Text = L"Close";
				 }
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 1)
			{
				this->label5->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].name.c_str());
				this->label5->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1.size()) >= 1)
				{
					this->label11->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1[0]));
					this->label12->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results2[0]));
					this->label11->Visible = true;
					this->label12->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1.size()) >= 2)
				{
					this->label13->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1[1]));
					this->label15->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results2[1]));
					this->label13->Visible = true;
					this->label15->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1.size()) >= 3)
				{
					this->label14->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results1[2]));
					this->label16->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[0].results2[2]));
					this->label14->Visible = true;
					this->label16->Visible = true;
				}
			}
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 2)
			{
				this->label6->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].name.c_str());
				this->label6->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1.size()) >= 1)
				{
					this->label17->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1[0]));
					this->label20->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results2[0]));
					this->label17->Visible = true;
					this->label20->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1.size()) >= 2)
				{
					this->label18->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1[1]));
					this->label21->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results2[1]));
					this->label18->Visible = true;
					this->label21->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1.size()) >= 3)
				{
					this->label19->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results1[2]));
					this->label22->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[1].results2[2]));
					this->label19->Visible = true;
					this->label22->Visible = true;
				}
			}
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 3)
			{
				this->label7->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].name.c_str());
				this->label7->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1.size()) >= 1)
				{
					this->label23->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1[0]));
					this->label26->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results2[0]));
					this->label23->Visible = true;
					this->label26->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1.size()) >= 2)
				{
					this->label24->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1[1]));
					this->label27->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results2[1]));
					this->label24->Visible = true;
					this->label27->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1.size()) >= 3)
				{
					this->label25->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results1[2]));
					this->label28->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[2].results2[2]));
					this->label25->Visible = true;
					this->label28->Visible = true;
				}
			}
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 4)
			{
				this->label8->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].name.c_str());
				this->label8->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1.size()) >= 1)
				{
					this->label29->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1[0]));
					this->label32->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results2[0]));
					this->label29->Visible = true;
					this->label32->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1.size()) >= 2)
				{
					this->label30->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1[1]));
					this->label33->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results2[1]));
					this->label30->Visible = true;
					this->label33->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1.size()) >= 3)
				{
					this->label31->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results1[2]));
					this->label34->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[3].results2[2]));
					this->label31->Visible = true;
					this->label34->Visible = true;
				}
			}
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 5)
			{
				this->label9->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].name.c_str());
				this->label9->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1.size()) >= 1)
				{
					this->label35->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1[0]));
					this->label38->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results2[0]));
					this->label35->Visible = true;
					this->label38->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1.size()) >= 2)
				{
					this->label36->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1[1]));
					this->label39->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results2[1]));
					this->label36->Visible = true;
					this->label39->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1.size()) >= 3)
				{
					this->label37->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results1[2]));
					this->label40->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[4].results2[2]));
					this->label37->Visible = true;
					this->label40->Visible = true;
				}
			}
			if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w.size()) >= 6)
			{
				this->label10->Text = gcnew String(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].name.c_str());
				this->label10->Visible = true;
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1.size()) >= 1)
				{
					this->label41->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1[0]));
					this->label44->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results2[0]));
					this->label41->Visible = true;
					this->label44->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1.size()) >= 2)
				{
					this->label42->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1[1]));
					this->label45->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results2[1]));
					this->label42->Visible = true;
					this->label45->Visible = true;
				}
				if((user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1.size()) >= 3)
				{
					this->label43->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results1[2]));
					this->label46->Text = gcnew String(Convert::ToString(user::GetInstance()->GetPrevRoutine().r[workoutNum].w[5].results2[2]));
					this->label43->Visible = true;
					this->label46->Visible = true;
				}
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label21;
	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::Label^  label23;
	private: System::Windows::Forms::Label^  label24;
	private: System::Windows::Forms::Label^  label25;
	private: System::Windows::Forms::Label^  label26;
	private: System::Windows::Forms::Label^  label27;
	private: System::Windows::Forms::Label^  label28;
	private: System::Windows::Forms::Label^  label29;
	private: System::Windows::Forms::Label^  label30;
	private: System::Windows::Forms::Label^  label31;
	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::Label^  label34;
	private: System::Windows::Forms::Label^  label35;
	private: System::Windows::Forms::Label^  label36;
	private: System::Windows::Forms::Label^  label37;
	private: System::Windows::Forms::Label^  label38;
	private: System::Windows::Forms::Label^  label39;
	private: System::Windows::Forms::Label^  label40;
	private: System::Windows::Forms::Label^  label41;
	private: System::Windows::Forms::Label^  label42;
	private: System::Windows::Forms::Label^  label43;
	private: System::Windows::Forms::Label^  label44;
	private: System::Windows::Forms::Label^  label45;
	private: System::Windows::Forms::Label^  label46;
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->label29 = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->label31 = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->label36 = (gcnew System::Windows::Forms::Label());
			this->label37 = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label39 = (gcnew System::Windows::Forms::Label());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->label41 = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->label45 = (gcnew System::Windows::Forms::Label());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(99, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Previous Exercises:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(381, 13);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(45, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Results:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(339, 36);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"label3";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(431, 36);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(35, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"label4";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(16, 79);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(35, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"label5";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(16, 163);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(35, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"label6";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(16, 255);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(35, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"label7";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(16, 345);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(35, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"label8";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(16, 438);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(35, 13);
			this->label9->TabIndex = 8;
			this->label9->Text = L"label9";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(16, 522);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(41, 13);
			this->label10->TabIndex = 9;
			this->label10->Text = L"label10";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(333, 79);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(41, 13);
			this->label11->TabIndex = 10;
			this->label11->Text = L"label11";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(431, 79);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(41, 13);
			this->label12->TabIndex = 11;
			this->label12->Text = L"label12";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(333, 102);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(41, 13);
			this->label13->TabIndex = 12;
			this->label13->Text = L"label13";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(333, 126);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(41, 13);
			this->label14->TabIndex = 13;
			this->label14->Text = L"label14";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(431, 102);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(41, 13);
			this->label15->TabIndex = 14;
			this->label15->Text = L"label15";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(431, 126);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(41, 13);
			this->label16->TabIndex = 15;
			this->label16->Text = L"label16";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(333, 163);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(41, 13);
			this->label17->TabIndex = 16;
			this->label17->Text = L"label17";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(333, 185);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(41, 13);
			this->label18->TabIndex = 17;
			this->label18->Text = L"label18";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(333, 208);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(41, 13);
			this->label19->TabIndex = 18;
			this->label19->Text = L"label19";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(431, 163);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(41, 13);
			this->label20->TabIndex = 19;
			this->label20->Text = L"label20";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(431, 185);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(41, 13);
			this->label21->TabIndex = 20;
			this->label21->Text = L"label21";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(431, 208);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(41, 13);
			this->label22->TabIndex = 21;
			this->label22->Text = L"label22";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(333, 255);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(41, 13);
			this->label23->TabIndex = 22;
			this->label23->Text = L"label23";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(333, 280);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(41, 13);
			this->label24->TabIndex = 23;
			this->label24->Text = L"label24";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(333, 304);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(41, 13);
			this->label25->TabIndex = 24;
			this->label25->Text = L"label25";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(431, 255);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(41, 13);
			this->label26->TabIndex = 25;
			this->label26->Text = L"label26";
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(431, 280);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(41, 13);
			this->label27->TabIndex = 26;
			this->label27->Text = L"label27";
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(431, 304);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(41, 13);
			this->label28->TabIndex = 27;
			this->label28->Text = L"label28";
			// 
			// label29
			// 
			this->label29->AutoSize = true;
			this->label29->Location = System::Drawing::Point(333, 345);
			this->label29->Name = L"label29";
			this->label29->Size = System::Drawing::Size(41, 13);
			this->label29->TabIndex = 28;
			this->label29->Text = L"label29";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(333, 368);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(41, 13);
			this->label30->TabIndex = 29;
			this->label30->Text = L"label30";
			// 
			// label31
			// 
			this->label31->AutoSize = true;
			this->label31->Location = System::Drawing::Point(333, 392);
			this->label31->Name = L"label31";
			this->label31->Size = System::Drawing::Size(41, 13);
			this->label31->TabIndex = 30;
			this->label31->Text = L"label31";
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(431, 345);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(41, 13);
			this->label32->TabIndex = 31;
			this->label32->Text = L"label32";
			// 
			// label33
			// 
			this->label33->AutoSize = true;
			this->label33->Location = System::Drawing::Point(431, 368);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(41, 13);
			this->label33->TabIndex = 32;
			this->label33->Text = L"label33";
			// 
			// label34
			// 
			this->label34->AutoSize = true;
			this->label34->Location = System::Drawing::Point(431, 392);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(41, 13);
			this->label34->TabIndex = 33;
			this->label34->Text = L"label34";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(333, 438);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(41, 13);
			this->label35->TabIndex = 34;
			this->label35->Text = L"label35";
			// 
			// label36
			// 
			this->label36->AutoSize = true;
			this->label36->Location = System::Drawing::Point(333, 460);
			this->label36->Name = L"label36";
			this->label36->Size = System::Drawing::Size(41, 13);
			this->label36->TabIndex = 35;
			this->label36->Text = L"label36";
			// 
			// label37
			// 
			this->label37->AutoSize = true;
			this->label37->Location = System::Drawing::Point(333, 482);
			this->label37->Name = L"label37";
			this->label37->Size = System::Drawing::Size(41, 13);
			this->label37->TabIndex = 36;
			this->label37->Text = L"label37";
			// 
			// label38
			// 
			this->label38->AutoSize = true;
			this->label38->Location = System::Drawing::Point(431, 438);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(41, 13);
			this->label38->TabIndex = 37;
			this->label38->Text = L"label38";
			// 
			// label39
			// 
			this->label39->AutoSize = true;
			this->label39->Location = System::Drawing::Point(431, 460);
			this->label39->Name = L"label39";
			this->label39->Size = System::Drawing::Size(41, 13);
			this->label39->TabIndex = 38;
			this->label39->Text = L"label39";
			// 
			// label40
			// 
			this->label40->AutoSize = true;
			this->label40->Location = System::Drawing::Point(431, 482);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(41, 13);
			this->label40->TabIndex = 39;
			this->label40->Text = L"label40";
			// 
			// label41
			// 
			this->label41->AutoSize = true;
			this->label41->Location = System::Drawing::Point(333, 522);
			this->label41->Name = L"label41";
			this->label41->Size = System::Drawing::Size(41, 13);
			this->label41->TabIndex = 40;
			this->label41->Text = L"label41";
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Location = System::Drawing::Point(333, 545);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(41, 13);
			this->label42->TabIndex = 41;
			this->label42->Text = L"label42";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(333, 568);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(41, 13);
			this->label43->TabIndex = 42;
			this->label43->Text = L"label43";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(431, 522);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(41, 13);
			this->label44->TabIndex = 43;
			this->label44->Text = L"label44";
			// 
			// label45
			// 
			this->label45->AutoSize = true;
			this->label45->Location = System::Drawing::Point(431, 545);
			this->label45->Name = L"label45";
			this->label45->Size = System::Drawing::Size(41, 13);
			this->label45->TabIndex = 44;
			this->label45->Text = L"label45";
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(431, 568);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(41, 13);
			this->label46->TabIndex = 45;
			this->label46->Text = L"label46";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(447, 599);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(81, 23);
			this->button1->TabIndex = 46;
			this->button1->Text = L"Next Workout";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &previousWorkouts::button1_Click);
			// 
			// previousWorkouts
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(540, 634);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label46);
			this->Controls->Add(this->label45);
			this->Controls->Add(this->label44);
			this->Controls->Add(this->label43);
			this->Controls->Add(this->label42);
			this->Controls->Add(this->label41);
			this->Controls->Add(this->label40);
			this->Controls->Add(this->label39);
			this->Controls->Add(this->label38);
			this->Controls->Add(this->label37);
			this->Controls->Add(this->label36);
			this->Controls->Add(this->label35);
			this->Controls->Add(this->label34);
			this->Controls->Add(this->label33);
			this->Controls->Add(this->label32);
			this->Controls->Add(this->label31);
			this->Controls->Add(this->label30);
			this->Controls->Add(this->label29);
			this->Controls->Add(this->label28);
			this->Controls->Add(this->label27);
			this->Controls->Add(this->label26);
			this->Controls->Add(this->label25);
			this->Controls->Add(this->label24);
			this->Controls->Add(this->label23);
			this->Controls->Add(this->label22);
			this->Controls->Add(this->label21);
			this->Controls->Add(this->label20);
			this->Controls->Add(this->label19);
			this->Controls->Add(this->label18);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"previousWorkouts";
			this->Text = L"Previous Workout Results";
			this->Load += gcnew System::EventHandler(this, &previousWorkouts::previousWorkouts_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void previousWorkouts_Load(System::Object^  sender, System::EventArgs^  e) {
				 totalWorkouts = user::GetInstance()->getNumWorkouts();

				if(user::GetInstance()->GetPrevRoutineCardio(workoutNumber))				 
				//if((user::GetInstance()->GetPrevRoutine().r[workoutNumber].w[0].cardio) == 0)
				 {
					 this->label3->Text = L"Time:";
					 this->label4->Text = L"Distance:";
					 this->label3->Visible = true;
					 this->label4->Visible = true;
				 }
				 else
				 {
					 this->label3->Text = L"Reps";
					 this->label4->Text = L"Weight:";
					 this->label3->Visible = true;
					 this->label4->Visible = true;
				 }
			 }
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				 workoutNumber++;
				 if(this->button1->Text == L"Close")
				 {
					 this->Close();
				 }
				 else
				 {
					 hideLabels();
					 displayData(workoutNumber);
				if(user::GetInstance()->GetPrevRoutineCardio(workoutNumber))				 
				//if((user::GetInstance()->GetPrevRoutine().r[workoutNumber].w[0].cardio) == 0)
				 {
					 this->label3->Text = L"Time:";
					 this->label4->Text = L"Distance:";
					 this->label3->Visible = true;
					 this->label4->Visible = true;
				 }
				 else
				 {
					 this->label3->Text = L"Reps";
					 this->label4->Text = L"Weight:";
					 this->label3->Visible = true;
					 this->label4->Visible = true;
				 }
				 }
				 if(workoutNumber >= (totalWorkouts - 1))
				 {
					 this->button1->Text = L"Close";
				 }
				 //Even though this code is fine, it shows exercise names with blank results for 1 or 2 extra pages...
				 //Commented it out and used the above code instead.
				 /*if(workoutNumber >= totalWorkouts)
				 {
					 this->button1->Text = L"Close";
				 }*/
			 }
};
}
