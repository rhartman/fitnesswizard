#include "enterResults.h"
#include "WelcomeScreen.h"
#pragma once
namespace CEN4010_PROJECT3222012 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;
	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  txtUsername;
	private: System::Windows::Forms::TextBox^  txtPassword;
	protected: 

	protected: 

	protected: 

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  btnLogin;

	private: System::Windows::Forms::Button^  btnNewUser;



	private: System::Windows::Forms::Label^  label3;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->txtUsername = (gcnew System::Windows::Forms::TextBox());
			this->txtPassword = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->btnLogin = (gcnew System::Windows::Forms::Button());
			this->btnNewUser = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// txtUsername
			// 
			this->txtUsername->AcceptsReturn = true;
			this->txtUsername->Location = System::Drawing::Point(210, 91);
			this->txtUsername->Name = L"txtUsername";
			this->txtUsername->Size = System::Drawing::Size(100, 20);
			this->txtUsername->TabIndex = 0;
			this->txtUsername->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtUsername_KeyPress);
			// 
			// txtPassword
			// 
			this->txtPassword->Location = System::Drawing::Point(210, 146);
			this->txtPassword->Name = L"txtPassword";
			this->txtPassword->Size = System::Drawing::Size(100, 20);
			this->txtPassword->TabIndex = 1;
			this->txtPassword->UseSystemPasswordChar = true;
			this->txtPassword->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::txtPassword_KeyPress);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(207, 75);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Username:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(207, 130);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(56, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Password:";
			// 
			// btnLogin
			// 
			this->btnLogin->Location = System::Drawing::Point(164, 211);
			this->btnLogin->Name = L"btnLogin";
			this->btnLogin->Size = System::Drawing::Size(75, 23);
			this->btnLogin->TabIndex = 4;
			this->btnLogin->Text = L"Login";
			this->btnLogin->UseVisualStyleBackColor = true;
			this->btnLogin->Click += gcnew System::EventHandler(this, &Form1::btnLogin_Click);
			// 
			// btnNewUser
			// 
			this->btnNewUser->Location = System::Drawing::Point(283, 211);
			this->btnNewUser->Name = L"btnNewUser";
			this->btnNewUser->Size = System::Drawing::Size(75, 23);
			this->btnNewUser->TabIndex = 5;
			this->btnNewUser->Text = L"New User";
			this->btnNewUser->UseVisualStyleBackColor = true;
			this->btnNewUser->Click += gcnew System::EventHandler(this, &Form1::btnNewUser_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(13, 13);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(508, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"For new users, please enter desired username and password and click the \"New User" 
				L"\" button to continue.";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(534, 262);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->btnNewUser);
			this->Controls->Add(this->btnLogin);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->txtPassword);
			this->Controls->Add(this->txtUsername);
			this->Name = L"Form1";
			this->Text = L"Please Log In";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void btnNewUser_Click(System::Object^  sender, System::EventArgs^  e) {
			 //get the username from the textbox and add it to the user object
			 user::GetInstance()->SetUsername(misc::TextboxToString(txtUsername));
			 //get the password from the textbox and add it to the user object
			 user::GetInstance()->SetPassword(misc::TextboxToString(txtPassword));

			 //clear the password so no one can get in by accedent
			 txtPassword->Text = gcnew String("");
			 //create the new user form
			 NewUser ^ form = gcnew NewUser;

			 //show the new window
			 form->ShowDialog();
		 }
private: System::Void btnLogin_Click(System::Object^  sender, System::EventArgs^  e) {
			 /*If there is no file in the build directory whose name matches what's in the 
			 Username field play obnoxious sound and show error message.*/
			 if(!File::Exists(Directory::GetCurrentDirectory() + "/" + txtUsername->Text))
			 {
				 /*SoundPlayer ^ player = gcnew SoundPlayer;
				 player->SoundLocation = Directory::GetCurrentDirectory() + "/pwhistle.wav";
				 player->Load();
				 player->Play();*/
				 MessageBox::Show("That username doesn't exist.\nPLease click the \"New User\" button to create a new user.");
			 }
			 //Otherwise tell the user that the login function doesn't exist yet.
			 else
			 {
				 /*SoundPlayer ^ player = gcnew SoundPlayer;
				 player->SoundLocation = Directory::GetCurrentDirectory() + "/cosby.wav";
				 player->Load();
				 player->Play();*/
				 user::GetInstance()->SetUsername(misc::TextboxToString(txtUsername));
				 user::GetInstance()->load();
				 if(String::Compare(misc::getMD5String(txtPassword->Text),gcnew String(user::GetInstance()->GetPassword().c_str()))==0)
				 {
					 WelcomeScreen ^form = gcnew WelcomeScreen;
					 form->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Form1::closeForm);
					 form->Show();
					 this->txtPassword->Text = L"";
					 this->txtUsername->Text = L"";
				 this->Hide();
				 }else{
					 MessageBox::Show("password incorrect");
				 }
			 }
		 }

private: System::Void closeForm(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 this->Show();
		 }
private: System::Void txtPassword_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 if(e->KeyChar == '\r')
			 {
				 btnLogin_Click(sender, e);
			 }
		 }
private: System::Void txtUsername_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			 if(e->KeyChar == '\r')
			 {
				 this->txtPassword->Focus();
			 }
		 }
};
}

